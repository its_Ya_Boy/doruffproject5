# ***ReadMe for Project Five Iteration 3***

## ***Overview***

pie.go will estimate pie by throwing darts at the upper right quadrant of
a circle.  The pie estimation is done concurrently in a predefined manner.  
The results will show the time it took, it's estimation, number of darts thrown,
and its delta from the mathematical Pi.

## ***Organization***
The flow of the program is written below:

main -> printTimeConcurrent -> estimatePieConcurrently -> end

## ***How to use:***

  1) Type in terminal and cd into the folder containing pie.go

  2) terminal command: go run pie.go


## ***Files:***

***pie.go***: Main file calls main function which creates an anonymous Function
that calls both printTime and estimatePie

  ***Functions***

  main = main function which creates an anonymous Function that calls both
  printTime() and estimatePie().

  printTime = Prints the number of darts, time elapsed calculating, pie
  calculation, and its associated delta from pie.

  estimatePie = Calculates pie with given number of darts.

  printTimeConcurrent = Prints the number of darts, time elapsed calculating,
  pie
  calculation, its associated delta from pie, and how many channels were used.

  estimatePieConcurrently = Calculates pie with given number of darts.

  makeChannel = makes the appropriate amount of channels for throwing darts.

## ***Test Runs:***

Results can be viewed in the pdf in the repository named: results.pdf.
