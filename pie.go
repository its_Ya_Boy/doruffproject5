package main

import (
	"fmt"
	"math/rand"
	"math"
	"sync"
	"time"
)
//Global waitgroup
var wg sync.WaitGroup

/*Main starts the program, creates a waitgroup, and calls printStuff
to estimate pie with different amounts of darts thrown.
*/
func main() {
	//function to print time elapsed, number of darts thrown, and the result of pie
	printStuff := func( targetDarts int, darts int, channel int) {
			printTimeConcurrent( time.Now(), targetDarts, channel, makeChannel(channel, darts))
			wg.Wait()
		}
	testVar:=[8]int{100, 1_000, 10_000, 100_000, 1_000_000, 10_000_000, 100_000_000, 1_000_000_000}
	for j := range testVar {
		//Number of dart throws
		channels := 1
		// number of throws per this iterations
		numThrows := testVar[j]/channels
		//for 9 iterations
		for i := 0; i < 6; i++ {
			if channels > testVar[j] {
				break
			}
			printStuff(testVar[j], numThrows, channels)
			channels*=10
			numThrows = testVar[j]/channels
		}
	}
	//Print when finished with the program
	fmt.Println("All Done")
}
/*Prints result of estimatePie, time elapsed, delta, and darts thrown
start: time starting operation
darts: darts to throw
evalPie: float64 of the answer
*/
func printTimeConcurrent(start time.Time, darts int, channel int, evalPie float64) {
		var elapsed = time.Since(start)
		fmt.Println("darts: ", darts)
		fmt.Println("Channels: ", channel)
		fmt.Println("With an answer of: ", evalPie)
		fmt.Println("time:", elapsed)
		//Calculate and print delta
		fmt.Println("With a delta of: ", math.Abs((math.Pi - evalPie)))
		fmt.Println("")
}

/*Prints result of estimatePie, time elapsed, delta, and darts thrown
start: time starting operation
darts: darts to throw
evalPie: float64 of the answer
*/
func printTime(start time.Time, darts int, evalPie float64) {
		var elapsed = time.Since(start)
		fmt.Println("darts: ", darts)
		fmt.Println("With an answer of: ", evalPie)
		fmt.Println("time:", elapsed)
		//Calculate and print delta
		fmt.Println("With a delta of: ", math.Abs((math.Pi - evalPie)))
		fmt.Println("")
}

/*Makes a channel and grabs the result of the throws from each to combine them for
an answer.
numChannel: number of channels for the test
numDarts: number of darts to throw each channel
Return: Pie estimation
*/
func makeChannel(numChannel int, numDarts int) float64{
	//Make channel
	channel:= make(chan int)
	//For how many channels there are throw the passed amount of darts
	for i:=0; i<numChannel;i++ {
		wg.Add(1)
		defer wg.Done()
		go estimatePieConcurrently(numDarts, channel)
	}
	//Store the result of pi
	pie:=0
	//For the number of channels made fill pie with the result
	for i:=0; i<numChannel;i++ {
		pie+=<-channel
	}
	//Inside proportion calulates the pie based on those hit and returns the value
	var insideProportion float64 = float64(pie) / float64(numDarts * numChannel)
	insideProportion*=4
	//Return proportion * 4
	return insideProportion
}


/*Estimates pie by throwing darts in the upper left quadrant concurrently with itself
numDartsToThrow: number of darts to throw
*/
func estimatePieConcurrently(numDartsToThrow int, channel chan int) {
	//Counter of hit darts
	numDartsInside := 0
	//for loop to throw as many darts as given
	for numDartsThrown := 0; numDartsThrown < numDartsToThrow; numDartsThrown++ {
			x := rand.Float64()
			y := rand.Float64()
			z := x * x + y * y
			if z < 1.0 {
				numDartsInside++
			}
	}
	//Return channel amount
	channel <- numDartsInside
}

/*Estimates pie by throwing darts in the upper left quadrant
numDartsToThrow: number of darts to throw
*/
func estimatePie(numDartsToThrow int) float64 {
	//Counter of hit darts
	numDartsInside := 0
	//for loop to throw as many darts as given
	for numDartsThrown := 0; numDartsThrown < numDartsToThrow; numDartsThrown++ {
			x := rand.Float64()
			y := rand.Float64()
			z := x * x + y * y
			if z < 1.0 {
				numDartsInside++
			}
	}
	//Inside proportion calulates the pie based on those hit and returns the value
	var insideProportion float64 = float64(numDartsInside) / float64(numDartsToThrow)
	insideProportion*=4
	//Return proportion * 4
	return insideProportion
}
